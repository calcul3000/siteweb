<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="style.css" type="text/css" media="screen" title="rouge" charset="utf-8" />
	<?php session_start();?>
</head>
<?php 
if(isset($_POST['niveau'])){
	$_SESSION['niveau']=$_POST["niveau"];
}
else{
	$_SESSION['niveau']="niveau1";
}
?>
<div>
<form method="post" action="calcul.php">
    <p>
		Calcul : 
		<?php
			if($_SESSION["niveau"] == "niveau1"){
				$x = rand(0, 10);
				$y = rand(0, 10);
				$valeurSigne = rand(0, 1);
				
				if ($valeurSigne == 0){
					$equation = "$x + $y";
					$calcul = $x + $y;
				}
				else{
					if ($x >= $y){
						$equation = "$x - $y";
						$calcul = $x - $y;
					}
					else{
						$equation = "$y - $x";
						$calcul = $y - $x;
					}
				}
			}
			else{
				$x = rand(0, 10);
				$y = rand(0, 10);
				$operation = rand(0,2);
				if ($operation == 0){
					$equation = "$x + $y";
					$calcul = $x + $y;
				}
				elseif($operation == 1){
					if ($x >= $y){
						$equation = "$x - $y";
						$calcul = $x - $y;
					}
					else{
						$equation = "$x - $y";
						$calcul = $y - $x;
					}
				}
				else{
					$equation = "$x * $y";
					$calcul = $x * $y;
				}
			}	
			echo $equation;
		?>
		<?php 
			$_SESSION['calcul'] = $calcul;
			$_SESSION['equation'] = $equation;
		?>
	</p>
	
    <p>
		Réponse : <input type="number" name="reponse" id="reponse" placeholder="Réponse" size="50" maxlength="50" /><br>
    </p>
	<p>
		<input type="submit" value="OK">
	</p>
</form>
<form action="index.php">
	<input type="submit" value="retour choix niveau">
</form>	
</div>

